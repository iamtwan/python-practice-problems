# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


# sum(via length?) of attendees
# is 50% or more of could use divide by 2 or multiply .5?
# sum(via length?) of members_list

def has_quorum(attendees_list, members_list):
    return len(attendees_list) >= (len(members_list) / 2)


attend = ['Alice', 'Bob', 'Charlie', 'David', 'Emily',
          'Frank', 'Grace', 'Henry', 'Isabella', 'Jack']
member = ['Alice', 'Bob', 'Charlie', 'David', 'Emily', 'Frank', 'Grace', 'Henry', 'Isabella', 'Jack',
          'Katherine', 'Liam', 'Mia', 'Noah', 'Olivia', 'Penelope', 'Quinn', 'Riley', 'Sophia', 'Hector', 'Tracy']
# attend = ['Alice', 'Bob', 'Charlie', 'David', 'Emily',
#           'Frank', 'Grace', 'Henry', 'Isabella', 'Jack']
# member = ['Alice', 'Bob', 'Charlie', 'David', 'Emily', 'Frank', 'Grace', 'Henry', 'Isabella', 'Jack',
#           'Katherine', 'Liam', 'Mia', 'Noah', 'Olivia', 'Penelope', 'Quinn', 'Riley', 'Sophia', 'Hector']
print(has_quorum(attend, member))
print(len(attend))
print(len(member))
