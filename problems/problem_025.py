# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None

def calculate_sum(values):
    if values == []:
        return None
    return sum(values)


print(calculate_sum([10, 20, 30, 40, 50, 60, 70, 80, 90, 100]))
print(calculate_sum([]))
