# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# **If the list of values is empty, the function should
# **return None
#
# **If the list of values has only one value, the function
# **should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# return sorted(values).pop(len(values) - 1)
# single line with pop no good.


def find_second_largest(values):
    if len(values) <= 1:
        return None
    return sorted(values)[-2]


print(find_second_largest([10, 50, 500, 6, 105, 664, 44, 320, 420, 203, 105]))
