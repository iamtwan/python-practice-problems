# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# sum the list of values
# mean(avg) function the sum of values - needs import ?
# or
# sum of the list divided by length of values list
# return none if list is empty


def calculate_average(values):
    if values == []:
        return None
    return sum(values) / len(values)


print(calculate_average([10, 20, 30, 40, 50, 60, 70, 80, 90, 100]))
print(calculate_average([]))
