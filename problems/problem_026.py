# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    grades_avg = sum(values) / len(values)
    if grades_avg >= 90:
        return 'A'
    elif grades_avg >= 80:
        return 'B'
    elif grades_avg >= 70:
        return 'C'
    elif grades_avg >= 60:
        return 'D'
    else:
        return 'F'


scores = [85, 85, 92, 74, 61, 82, 75, 62, 69,
          80, 77, 97, 98, 93, 71, 100, 98, 99, 92, 89]
print(calculate_grade([10, 20, 30, 40, 50, 60, 70, 80, 90, 100]))
print(calculate_grade(scores))
