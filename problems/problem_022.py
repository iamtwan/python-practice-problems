# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# if workday is true and day is not sunny - append in umbrella?
# if workday is true - append in laptop?
# if workday is false - append in surfboard

# gear_needed = []


def gear_for_day(is_workday, is_sunny):
    gear_needed = []
    if is_workday is True:
        gear_needed.append('laptop')
        if is_sunny is False:
            gear_needed.append('umbrella')
    else:
        gear_needed.append('surfboard')
    return gear_needed


print(gear_for_day(True, False))
print(gear_for_day(False, False))
print(gear_for_day(True, True))
print(gear_for_day(False, True))
# print(gear_needed)
