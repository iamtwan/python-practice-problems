# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == []:
        return None
    current_max = 0
    for nums in values:
        if nums > current_max:
            current_max = nums
    return current_max


print(max_in_list([101, 200, 30, 240, 540, 600, 70, 80, 705, 100]))
print(max_in_list([]))
