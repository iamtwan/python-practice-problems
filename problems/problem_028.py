# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if s == '':
        return None
    final_str = ''
    for letter in s:
        if letter not in final_str:
            final_str += letter
    return final_str

    # seen = set()
    # final_str = ''
    # for letter in s:
    #     if letter not in seen:
    #         final_str += letter
    #         seen.add(letter)
    # return final_str


print(remove_duplicate_letters('abccyyuiioolhgjadba'))
print(remove_duplicate_letters('abccccddbahjfg'))
print(remove_duplicate_letters('fghjteryovxbxvcbvvvbdfabccba'))
print(remove_duplicate_letters('mississippi'))
print(remove_duplicate_letters(''))
