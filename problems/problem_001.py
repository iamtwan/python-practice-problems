# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# if value1 is less than value2 return value1
# if value2 is less than value1 return value2
# if value1 and value2 are equal return either

def minimum_value(value1, value2):
    if value1 <= value2:
        return value1
    else:
        return value2


print(minimum_value(3, 3))
print(minimum_value(1, 5))
print(minimum_value(8, 2))
