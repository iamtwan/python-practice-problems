# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    new_dict = {}
    for prop in dictionary:
        new_dict.update({dictionary[prop]: prop})
    return new_dict


print(reverse_dictionary({"key": "value", "key2": "value2"}))
print(reverse_dictionary({"name": "Emma", "age": 27, "city": "New York",
      "hobby": "photography", "favorite_color": "blue"}))
print(reverse_dictionary({"one": 1, "two": 2, "three": 3}))
